#!/bin/bash

tmux new-window
tmux rename-window "Runner"

tmux send-keys 'source venv/bin/activate' C-m
tmux send-keys 'python3 run.py' C-m

tmux split-window -h -p 40 -c './app/static/css'

tmux send-keys 'sass --watch .:.' C-m

