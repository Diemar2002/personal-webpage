from __future__ import annotations

from typing import List
from flask import redirect, url_for
from flask.blueprints import Blueprint

import logging
log = logging.getLogger(__package__)

import os.path as pth
ROUTES_FOLDER = 'routes' 
ROUTES_DIR = pth.join(pth.dirname(pth.abspath(__file__)), ROUTES_FOLDER)


from dataclasses import dataclass

@dataclass
class Route:
	blueprint: Blueprint
	prefix: str

from pathlib import Path
import importlib
# Get all modules
def get_bp() -> Blueprint:
	def rec_routes_scanner(path: Path, root: Path) -> List[Route]:
		routes: List[Route] = []
		for descr in path.iterdir():
			if descr.is_file() and descr.suffix == '.py' and descr.name != '__init__.py':
				# Calculate package name
				relative_path = descr.relative_to(root.parent)
				package_name = '.'.join(relative_path.with_suffix('').parts)

				# Import the module
				mod = importlib.import_module(f'.{package_name}', package=__package__)
				
				# Calculate prefix
				prefix = '/' + '/'.join(relative_path.with_suffix('').parts[1:])

				if not hasattr(mod, 'bp') or not isinstance(mod.bp, Blueprint):
					log.error(f'Route {descr} does not contain a bp Blueprint variable')
				else:
					log.info(f'Adding routes for {prefix}')
					routes.append(Route(
						blueprint=mod.bp,
						prefix=prefix
					))
			elif descr.is_dir():
				routes.extend(rec_routes_scanner(descr, root))

		return routes

	routes_path = Path(ROUTES_DIR)
	routes = rec_routes_scanner(routes_path, routes_path)

	bp = Blueprint('main', __name__, url_prefix='/')
	for route in routes:
		bp.register_blueprint(route.blueprint, url_prefix=route.prefix)

	@bp.route('/')
	def _():
		return redirect('/index')

	return bp

