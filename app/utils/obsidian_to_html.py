from __future__ import annotations

from bs4 import BeautifulSoup
import functools

import logging
log = logging.getLogger('app')

SELECTORS_TO_REMOVE = [
	'.copy-code-button',
	'.heading-collapse-indicator',
	'.heading-after'
]

@functools.lru_cache()
def transform(html_content: str) -> str:
    log.info('Processing Obsidian HTML content')
    tree = BeautifulSoup(html_content, 'html.parser')
    doc = tree.select_one('.document-container')

    if doc is None:
        return '<p>Couldn\'t process document</p>'
    else:
        # Apply more filtering
        for selector in SELECTORS_TO_REMOVE:
            for elem in doc.select(selector):
                elem.decompose()

        for img in doc.select('img'):
            img['src'] = '/'.join(img['src'].split('/')[1:])
        return str(doc)
