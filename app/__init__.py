from flask import Flask, render_template

from . import logging
# Configure logger
logging.configure_logger(__package__, 'app.log')

import logging
log = logging.getLogger(__package__)

import os.path as pth
CURRENT_PATH = pth.dirname(pth.abspath(__file__))
STATIC_FOLDER = pth.join(CURRENT_PATH, 'static')
log.info(f'Using {STATIC_FOLDER} as static folder')

def create_app() -> Flask:
	app = Flask(__name__, static_folder=STATIC_FOLDER)

	app.config.from_pyfile('config.py')

	@app.after_request
	def set_response_headers(response):
		response.headers["Cross-Origin-Opener-Policy"] = "same-origin"
		response.headers["Cross-Origin-Embedder-Policy"] = "require-corp"
		return response

	@app.context_processor
	def inject_globals():
		##################
		# Configurations #
		##################
		return {
			'name': 'Laura Morales Román',
			'pronouns': '(she/her)'
		}

	@app.errorhandler(404)
	def not_found(e):
		return render_template('notfound.html')

	from . import router
	app.register_blueprint(router.get_bp())

	return app
