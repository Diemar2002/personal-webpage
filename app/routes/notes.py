# -*- coding: utf-8 -*-
import base64
import unicodedata
import bs4
from flask import Blueprint, current_app, render_template, request, url_for

bp = Blueprint('notes', __name__)

import os.path as pth
import functools

from app.utils.obsidian_to_html import transform

import logging
log = logging.getLogger(__package__)

def get_real_path(url):
    rel_path = url.lstrip('/')  # Remove the leading slash
    return pth.join(current_app.root_path, rel_path).replace('%20', ' ')

@bp.route('/<path:folder>/<note>')
def render(folder: str, note: str):
	# Get folder
	try:
		base_uri = url_for('static', filename=pth.join('content', folder))
		folder_path = get_real_path(base_uri)
		log.info(f'Reading {note} in {folder_path}')
		file_path = pth.join(folder_path, note)
		file = open(file_path)

		title = request.args.get('title', note.split('.')[0])

		content = transform(file.read())
		content = common_transform(content, base_uri)

		return render_template('note.html', 
						 content=content,
						 title=title
		)
	except FileNotFoundError as e:
		return (render_template('generic_error.html', 
			err_title="Note doesn't exist",
			err_description=f'File {note} in {folder} was not found'
		), 404)
	except Exception as e:
		return (render_template('generic_error.html', 
			err_title='500 Server error',
						  err_description=f'{e.__class__}: {e}'
		  ), 500)

@functools.lru_cache()
def common_transform(html_content: str, base_path: str) -> str:
	tree = bs4.BeautifulSoup(html_content, 'html.parser')

	for img in tree.select('img'):
		img['src'] = f'{base_path}/{img["src"]}'

	for snippet in tree.select('code[class^=language-]'):
		normalized = ''.join(
			c for c in unicodedata.normalize('NFD', snippet.text)
			if unicodedata.category(c) != 'Mn'
		)

		code = base64.urlsafe_b64encode(normalized.encode('utf-8')).decode('ascii')

		button = tree.new_tag('a', href=f'/content/course_runner?code={code}', attrs={'class': 'runner'})
		button.string = 'Enviar al entorno '
		icon = tree.new_tag('i', attrs={'class': 'fa fa-person-running'})
		button.append(icon) 


		if snippet.parent is not None:
			snippet.parent.append(button)

	return str(tree)
