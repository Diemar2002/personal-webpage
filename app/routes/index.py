from typing import List
from flask import Blueprint, render_template

bp = Blueprint('index', __name__)

from dataclasses import dataclass
from enum import Enum

class Level(Enum):
	HIGH = 'high'
	MEDIUM = 'medium'
	LOW = 'low'

@dataclass
class Skill:
	name: str
	icon: str = ''
	level: Level = Level.LOW

SKILLS: List[Skill] = [
	Skill(
		name='C',
		icon='devicon-c-plain',
		level=Level.HIGH
	),
	Skill(
		name='C++',
		icon='devicon-cplusplus-plain',
		level=Level.HIGH
	),
	Skill(
		name='Python',
		icon='devicon-python-plain',
		level=Level.HIGH
	),
	Skill(
		name='Lua',
		icon='devicon-lua-plain',
		level=Level.MEDIUM
	),
	Skill(
		name='Rust',
		icon='devicon-rust-plain',
		level=Level.LOW
	),
	Skill(
		name='Haskell',
		icon='devicon-haskell-plain',
		level=Level.LOW
	),
	Skill(
		name='JavaScript',
		icon='devicon-javascript-plain',
		level=Level.MEDIUM
	),
	Skill(
		name='TypeScript',
		icon='devicon-typescript-plain',
		level=Level.MEDIUM
	),
	Skill(
		name='Java',
		icon='devicon-java-plain',
		level=Level.MEDIUM
	),
	Skill(
		name='Flask',
		icon='devicon-flask-plain',
		level=Level.MEDIUM
	),
	Skill(
		name='React',
		icon='devicon-react-plain',
		level=Level.MEDIUM
	),
	Skill(
		name='Docker',
		icon='devicon-docker-plain',
		level=Level.HIGH
	),
	Skill(
		name='JQuery',
		icon='devicon-jquery-plain',
		level=Level.HIGH
	),
	Skill(
		name='Embedded (AVR, Espressif)',
		icon='fa fa-microchip',
		level=Level.HIGH
	),
	Skill(
		name='Git',
		icon='devicon-git-plain',
		level=Level.MEDIUM
	),
	Skill(
		name='Linux',
		icon='devicon-linux-plain',
		level=Level.HIGH
	),
	Skill(
		name='Postgres',
		icon='devicon-postgresql-plain',
		level=Level.LOW
	),
	Skill(
		name='Electronics',
		icon='fa fa-microchip',
		level=Level.MEDIUM
	),
	Skill(
		name='CAD',
		icon='fa fa-draw-polygon',
		level=Level.MEDIUM
	),
	Skill(
		name='Blender',
		icon='devicon-blender-original',
		level=Level.LOW
	),
	Skill(
		name='Education',
		icon='fa fa-graduation-cap',
		level=Level.MEDIUM
	),
]

@bp.route('/')
def index():
	print(Level.LOW.value)
	return render_template('index.html',
				skills=SKILLS
			)
