from __future__ import annotations
from flask import Blueprint, render_template, request

bp = Blueprint('courserunner', __name__)

@bp.route('/')
def runner():
	code = request.args.get('code', None)
	code = None if code is '' else code

	return render_template('content/courserunner.html', code=code)
