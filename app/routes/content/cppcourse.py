from __future__ import annotations
from typing import Dict
from flask import Blueprint, render_template

bp = Blueprint('cppcourse', __name__)

COURSE_PARTS: Dict[str, str] = {
	'Tipos, expresiones y entrada-salida': 'io-expr',
	'Tipos y representación en un ordenador': 'tipos-y-representacion-en-un-ordenador',
	'Tipos de datos compuestos': 'tipos-de-datos-compuestos',
	'Estructuras de control': 'estructuras-de-control',
	'Funciones': 'funciones',
	'Punteros': 'punteros',
	'Contenedores estándar': 'contenedores',
}

@bp.route('/')
def course():
	return render_template('content/cppcourse.html', 
						parts=COURSE_PARTS
	)
