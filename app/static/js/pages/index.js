
// Load images and lights
window.addEventListener('DOMContentLoaded', () => {

	// Colors
	// const colors = [
	//   '#dae0e5', '#FB6396', '#94CF95', 
	//   '#6EC1D6', '#CD84C8', '#7FE4D6', '#F92D72', '#6CCB6E',
	//   '#F26190', '#4CB9D6', '#C269BC', '#58D6BF'
	// ];
	const colors = [
		'#FB6396', '#6EC1D6', '#CD84C8', '#F26190', '#4CB9D6', '#C269BC'
	];

	document.querySelectorAll('.light').forEach((light) => {
		// Assign random position, size, and color
	  light.style.left = (Math.random() * 50 - 25) + '%';
	  light.style.top = (Math.random() * 50 - 25) + '%';
	  const color = colors[Math.floor(Math.random() * colors.length)];
	  let size = Math.random() * 10 + 40;
	  // size *= 2;
	  console.log(light);
	  light.style.backgroundImage = `radial-gradient(circle, ${color} 0%, transparent ${size}%)`;
  });


  const createAnimation = (name, x, y) => {
	  let styleSheet = document.styleSheets[0];
	  let keyframes =
		  `@keyframes ${name} {
0% { transform: translate(0, 0); }
100% { transform: translate(${x}vmax, ${y}vmax); }
}`;

	  styleSheet.insertRule(keyframes, styleSheet.cssRules.length);
  };

  document.querySelectorAll('.light').forEach((light, i) => {
	  // Adjust position calculation to make sure lights spawn inside parent div
	  // const color = colors[Math.floor(Math.random() * colors.length)];

	  // Create a new div within each .light div for the radial gradient
	  const innerDiv = document.createElement('div');
	  // innerDiv.style.backgroundImage = `radial-gradient(circle, ${color} 0%, transparent 80%)`;
	  light.appendChild(innerDiv);


	  // Calculate a random direction (angle) for the animation
	  const angle = Math.random() * 2 * Math.PI;
	  // Ensure the light moves at least 25vmax from its start position
	  const x = 25 * Math.cos(angle);
	  const y = 25 * Math.sin(angle);
	  const animationName = `move-randomly-${i}`;

	  createAnimation(animationName, x, y);

	  light.style.animation = `${animationName} ${Math.random() * 10 + 10}s ease-out infinite alternate`;
  });

});
